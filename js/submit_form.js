jQuery(document).ready(function($) {

	$('#myForm').on('submit', function(e){
        var form = $(this);
        var url = form.prop('action');
        $('#div_form').hide();
        $('#loading').show();
        $.ajax({
            url:url,
            data:form.serialize(),
            type: 'POST',
            dataType: "json",
            success: function(data){
                if(data == "true") {
                	$('#loading').hide();
                	$('#after-submit').show();
                }
                if (data == "false") {
                	$('#loading').hide();
                	$('#div_form').show();
                	$('#codigo_uso').show();
                }
            }
        });
        return false;
    });
});
