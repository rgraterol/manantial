jQuery(document).ready(function() {

	$('#codigo').blur(function(){
		var data = $(this).val();
        $.ajax({
          type: "POST",
          dataType: "json",
          url: "validates_code.php",
          data: {
            'codigo' : data
          },
          success: function(data) {
            if(data!= '0') {
            	$('#boton-enviar').prop("disabled", true)
            	$('#codigo-info').show();
            }
          },
          error: function(xhr, desc, err) {
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
          }
        });
	});

	$('#codigo').keyup(function(){
		$('#codigo-info').hide();
		$('#boton-enviar').prop("disabled", false);
	});



});