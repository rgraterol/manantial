jQuery(document).ready(function() {
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);

    var disableddates = [];
    $('#comuna_select').change(function() {
        var data = $(this).val();
        $.ajax({
          type: "POST",
          dataType: "json",
          url: "validates_date.php",
          data: {
            'comuna' : data
          },
          success: function(data) {
            disableddates = data;
            $("#datepicker").removeAttr("disabled");
          },
          error: function(xhr, desc, err) {
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
          }
        });
    });

    var currentDate = new Date();
    

    function DisableSpecificDates(date) {
         var m = date.getMonth();
         var d = date.getDate();
         var y = date.getFullYear();
         
         // First convert the date in to the mm-dd-yyyy format 
         // Take note that we will increment the month count by 1 
         var currentdate = d + '-' + (m + 1) + '-' + y ;
         
          // We will now check if the date belongs to disableddates array 
         for (var i = 0; i < disableddates.length; i++) {
            console.log()
             // Now check if the current date is in disabled dates array. 
             if ($.inArray(currentdate, disableddates) != -1 ) {
                return [false];
             }
             else {
                 return [date.getDay() != 0, ''];
             }
         }
    }

    $("#datepicker").datepicker({
        dateFormat: 'dd-mm-yy',
        defaultdate: currentDate,
        changeMonth: true,
        changeYear: true,
        yearRange: "-2:+2",
        beforeShowDay: DisableSpecificDates,
        minDate: '-1D',
        maxDate: '+30D',
    });   

});