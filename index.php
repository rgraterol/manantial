<?php     date_default_timezone_set('America/Santiago'); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Manantial | Promo Pack</title>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <link href="css/bootstrap-datepicker-xavier.css" rel="stylesheet">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <!--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script> 
        <script src="http://malsup.github.com/jquery.form.js"></script> 
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div id="div_form">
                    <form role="form" id="myForm" action="process.php" method="post">
                        <div class="col-md-12 form dinR">
                            <div class= "row" style="display:none" id="codigo_uso">
                                <div class="col-md-3 col-md-offset-3">
                                    <h1 class="text-danger"> Código en uso </h1>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 inputStyle">
                                    <label>RUT *</label>
                                    <input type="text" name="rut" placeholder="12.345.678-9" class="form-control" tabindex=1 id="txtRut" required/>
                                </div>
                                <div class="col-md-6 inputStyle">
                                    <label>Depto. o Villa *</label>
                                    <input type="text" name="depto" placeholder="Nro. Depto o Villa" tabindex=6 class="form-control" required/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 inputStyle">
                                    <label>Nombre Completo *</label>
                                    <input type="text" name="nombre" placeholder="José Contrera" tabindex=2 class="form-control" required/>
                                </div>
                                <div class="col-md-6 inputStyle">
                                    <label>Referencia / Intersección *</label>
                                    <input type="text" name="referencia" placeholder="Referencia o Intersección" tabindex=7 class="form-control"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 inputStyle">
                                    <label>Correo Electrónico *</label>
                                    <input type="email" name="mail" placeholder="usuario@correo.com" tabindex=3 class="form-control" required/>
                                </div>
                                <div class="col-md-6 inputStyle">
                                    <label>Comuna * **</label>
                                    <select name="comuna" class="form-control" tabindex=8 id="comuna_select" required />
                                    <option value="" disabled selected style="font-color: #ccc;">Comuna</option>
                                    <option>Santiago</option>
                                    <option>Cerrilos</option>
                                    <option>Cerro Navia</option>
                                    <option>Colina</option>
                                    <option>Conchalí</option>
                                    <option>El Bosque</option>
                                    <option>Estación Central</option>
                                    <option>Huechuraba</option>
                                    <option>Indepencia</option>
                                    <option>La Cisterna</option>
                                    <option>La Florida</option>
                                    <option>La Granja</option>
                                    <option>La Pintana</option>
                                    <option>La Reina</option>
                                    <option>Lampa</option>
                                    <option>Las Condes</option>
                                    <option>Lo Barnechea</option>
                                    <option>Lo Espejo</option>
                                    <option>Lo Prado</option>
                                    <option>Macul</option>
                                    <option>Maipú</option>
                                    <option>Ñuñoa</option>
                                    <option>Pedro Aguirre Cerda</option>
                                    <option>Peñalolén</option>
                                    <option>Providencia</option>
                                    <option>Pudahuel</option>
                                    <option>Puente Alto</option>
                                    <option>Quilicura</option>
                                    <option>Quinta Normal</option>
                                    <option>Recoleta</option>
                                    <option>Renca</option>
                                    <option>San Joaquín</option>
                                    <option>San Miguel</option>
                                    <option>San Ramón</option>
                                    <option>Vitacura</option>
                                    
                                    </select>  
                            </div>                    
                        </div>
                        <div class="row">
                            <div class="col-md-6 inputStyle">
                                <label>Teléfono de contacto *</label>
                                <input type="text" name="telefono" placeholder="02 212345678" tabindex=4 class="form-control" required/>
                            </div>
                            <div class="col-md-6 inputStyle">
                                <label>Seleccione fecha de despacho: *</label>
                                <input id="datepicker" type="text" name="fecha" class="form-control" tabindex=9 placeholder="Click para seleccionar fecha" required disabled="disabled"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 inputStyle">
                                <label>Dirección *</label>
                                <input type="text" name="direccion" placeholder="Avenida 1050" tabindex=5 class="form-control" required/>
                            </div>
                            <div class="col-md-6 inputStyle">
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 inputStyle">
                                <label> </label>
                                <input class="btns manbt" name="submit" type="submit" value="Enviar" id="boton-enviar"/>
                            </div>
                            <div class="col-md-6 inputStyle">
                                <label>* Campos Obligatorios <br /> ** Promoción valida solo para comunas aquí disponibles</label>
                            </div>
                        </div>
                        </div>
                    </form>
                </div>
                <div id="loading" style="display:none;">
                    <div class="col-md-12 form dinR">
                        <h1>
                            <i class="fa fa-spinner fa-spin"></i>
                        </h1>
                    </div>    
                </div>
                <div id="after-submit" class="after-submit" style="display:none;">
                    <div class="col-md-12 form dinR">
                        <h1> Gracias por su Pedido </h1>
                        <a href="http://www.manantial.com" class="btn btn-primary btn-large">Volver</a>
                    </div>
                </div>
            </div>
        </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!--<script src="js/bootstrap.min.js"></script>-->
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="js/validates_date.js"></script>
    <script src="js/submit_form.js"></script>

    </body>
</html>