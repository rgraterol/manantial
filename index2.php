<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Manantial | Promo Pack</title>
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <!--<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script> 
        <script src="http://malsup.github.com/jquery.form.js"></script> 
    </head>
    <body>


        <div class="container bg no-pad">
            <img src="img/bg.jpg" class="bg image-responsive">
            <div class="col-md-8 column formulario">
                <form role="form" id="myForm" action="process.php" method="post">
                    <div class="formu">
                        <input type="text" name="rut" placeholder="* RUT " class="form-control" id="txtRut" required/>
                        <label> </label>
                        <input type="text" name="nombre" placeholder="* Nombre Completo " class="form-control" required/>
                        <label> </label>
                        <input type="email" name="mail" placeholder="* Correo Electrónico " class="form-control" required/>
                        <label> </label>
                        <input type="text" name="telefono" placeholder="* Teléfono de contacto " class="form-control" required/>
                        <label> </label>
                        <input type="text" name="direccion" placeholder="* Dirección " class="form-control" required/>
                        <label> </label>
                        <center> <input class="btn btn-success manbt" name="submit" type="submit" value="Enviar" /></center> 

                    </div>

                    <div class="formu">
                        <input type="text" name="depto" placeholder="* Depto o Villa " class="form-control" required/>
                        <label> </label>
                        <input type="text" name="referencia" placeholder="Referencia / Intersección " class="form-control"/>
                        <label> </label>
                        <select name="comuna" placeholder="* Comuna "  class="form-control" required style="border: 1px solid #ccc; height 36px; border-radius: 0; background: #fff;"/>
                        <option value="" disabled selected style="font-color: #ccc;">* Comuna </option>
                        <option>Cerrilos</option>
                        <option>Cerro Navia</option>
                        <option>Conchalí</option>
                        <option>El Bosque</option>
                        <option>Estación Central</option>
                        <option>Huechuraba</option>
                        <option>Indepencia</option>
                        <option>La Cisterna</option>
                        <option>La Florida</option>
                        <option>La Granja</option>
                        <option>La Pintana</option>
                        <option>La Reina</option>
                        <option>Las Condes</option>
                        <option>Lo Barnechea</option>
                        <option>Lo Espejo</option>
                        <option>Lo Prado</option>
                        <option>Macul</option>
                        <option>Maipú</option>
                        <option>Ñuñoa</option>
                        <option>Padre Hurtado</option>
                        <option>Pedro Aguirre Cerda</option>
                        <option>Peñalolén</option>
                        <option>Pirque</option>
                        <option>Providencia</option>
                        <option>Pudahuel</option>
                        <option>Puente Alto</option>
                        <option>Quilicura</option>
                        <option>Quinta Normal</option>
                        <option>Recoleta</option>
                        <option>Renca</option>
                        <option>San Bernardo</option>
                        <option>San Joaquín</option>
                        <option>San José de Maipo</option>
                        <option>San Miguel</option>
                        <option>San Ramón</option>
                        <option>Santiago</option>
                        <option>Vitacura</option>
                        </select>
                    <label class="dateSelect"> Seleccione fecha de despacho: </label>
                    <input id="date" type="date" min="2015-06-10" max="2015-12-31" name="fecha" class="form-control" style="margin-top: -2.5% !important;">
                    <div class="campos">* Campos Obligatorios</div>
                    <script>
                        var date = document.querySelector('[type=date]');
                        function noMondays(e){
                            var day = new Date( e.target.value ).getUTCDay();
                            // Days in JS range from 0-6 where 0 is Sunday and 6 is Saturday
                            if( day ==  ){
                               e.target.setCustomValidity('Los días Domingos, no realizamos despachos, elija un nuevo día.');
                        } else {
                            e.target.setCustomValidity('');
                        }
                        }
                        date.addEventListener('input',noMondays);
                    </script>

                    </div>

                </form>
        </div>
        </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    </body>
</html>